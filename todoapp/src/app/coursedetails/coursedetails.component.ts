import { Component, OnInit } from '@angular/core';
import { Course } from '../models/course.model';
import { ActivatedRoute } from '@angular/router';
import { CourseService } from '../providers/course.service';

@Component({
  selector: 'app-coursedetails',
  templateUrl: './coursedetails.component.html',
  styleUrls: ['./coursedetails.component.css'],
})
export class CoursedetailsComponent implements OnInit {
  myCourse: Course = new Course(0, '', '', '', '', '', 0); // not an array

  constructor(
    private getACourse: CourseService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    //loadDetails(urlParams);
    console.log('I am here');
    // this is JS style
    // const urlParams = new URLSearchParams(location.search);

    // if (urlParams.has("courseid") === true) {

    //   this.id = Number(urlParams.get("courseid"));
    // }

    // here's Angular style:
    this.activatedRoute.queryParams.subscribe((params) => {
      let id = params['id'];
      this.getACourse.getACourse(id).subscribe((data) => {
        this.myCourse = data;
      });
    });
  }
}
