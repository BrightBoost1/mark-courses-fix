import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Todos } from '../models/todos.model';


@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(private http: HttpClient) { }


  private todoEndpoint: string = 'https://jsonplaceholder.typicode.com/todos';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getTodos() : Observable<Todos[]> {
    return this.http.get(this.todoEndpoint, this.httpOptions)
    .pipe(map(res => <Todos[]>res));
    }
}
