import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { UserService } from '../providers/user.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  myUsers: Array<User> = [];

  constructor(private getusers: UserService) { }

  ngOnInit(): void {
    this.getusers.getUsers().subscribe(data => {
      this.myUsers = data;
      });
  }

}
